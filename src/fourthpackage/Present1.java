package fourthpackage;

public class Present1 {
    public static void main(String[] args) {
// if statement
// syntax - if (boolean condition) {if true then whatever is inside this curly bracket will run}
// if its not true, it will skip to the next line (Keep in mind)
        String sky = "Blue";

        if (sky.equals("Blue")) {
            System.out.println("Good weather");
        }
        if (sky.equals("Black")) {
            System.out.println("Bad Weather");
        }

        int waist = 30;
        if (waist == 30) {
            System.out.println("Pants will fit");
        }
        if (waist == 40) {
            System.out.println("Pants will not fit");
        }

        boolean didYouEat = true;
        if (didYouEat) {
            System.out.println("Yes I ate");
        }
    }

}


// Syntax help
// For String we use (objectName.equals("data")) >> EQUAL TO
// For int we use (objectName == data)           >> EQUAL TO
// For boolean we use (objectName)               >> EQUAL TO
// For String we use (!objectName.equals("data"))>> NOT EQUAL TO
// For int we use (objectName != data)           >> NOT EQUAL TO
// For boolean we use (!objectName)              >> NOT EQUAL TO

// For String, equalsIgnoreCase >> ignores case sensitivity
// For String, equals           >> Doesn't ignore case sensitivity


