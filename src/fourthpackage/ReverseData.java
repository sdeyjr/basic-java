package fourthpackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ReverseData {
    public static void main(String[] args) {
        List<String> songs = new ArrayList<String>();
        songs.add("sultans of swing");
        songs.add("brothers in arm");
        songs.add("tears in heaven");
        songs.add("limelight");
        songs.add("layla");
        shuffleList(songs);
        System.out.println(songs);
    }

    public static void shuffleList(List<String> songs) {

        int n = songs.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(songs, i, change);
        }
    }
    public static void swap(List<String> songs, int i, int change) {
        String helper = songs.get(i);
        songs.set(i, songs.get(change));
        songs.set(change, helper);
    }
}
