package fourthpackage;

public class Present4 {
    public static void main(String[] args) {
        // Recap

        // The if statement is used in Java to run a block of code if a certain condition evaluates to be true
        // The else statement is used with an if statement to run code if a condition evaluates to be false
        // The if else if statement is used to evaluate multiple conditions >>
                // When the if statement is true, it will run the code within that block
                // When the if statement is not true, it will skip to the next line
                // When none of the if statements are deemed true, the code within the else statement will execute

        // The if statements are executed from the top going downward
        // The else statement is optional


        // Syntax
        // For String we use (objectName.equals("data")) >> EQUAL TO
        // For int we use (objectName == data)           >> EQUAL TO
        // For boolean we use (objectName)               >> EQUAL TO
        // For String we use (!objectName.equals("data"))>> NOT EQUAL TO
        // For int we use (objectName != data)           >> NOT EQUAL TO
        // For boolean we use (!objectName)              >> NOT EQUAL TO

        // For String, equalsIgnoreCase >> ignores case sensitivity
        // For String, equals           >> Doesn't ignore case sensitivity





    }
}
