package fourthpackage;

public class Present2 {
    public static void main(String[] args) {
        // else statement
        // is written after an if statement
        // has no conditon
        // is optional
        // will only execute when the "if" statement is false

        String sky = "Blue";
        if (sky.equals("black")) {
            System.out.println("Dont go out");
        } else {
            System.out.println("go out");
        }

        int waist = 32;
        if (waist == 30) {
            System.out.println("Fits me");
        } else {
            System.out.println("Doesnt fit me");
        }

        boolean didYouEat = false;
        if (didYouEat) {
            System.out.println("Yes I ate");
        } else {
            System.out.println("No I am hungry");
        }

    }
}
