package fourthpackage;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayListInsideHashMap {
    public static void main (String [] args) {

        ArrayList<HashMap<Integer, String>> barcelona = new ArrayList<>();

        HashMap <Integer, String> harselona = new HashMap<>();

        harselona.put (1, "Ter Stegen");
        harselona.put (2, "Dani Alves");
        harselona.put (3, "Gerard Pique");

        HashMap <Integer, String> winselona = new HashMap <>();

        winselona.put (6, "pedri");
        winselona.put (7, "gabi");
        winselona.put (9, "adama");

        barcelona.add (harselona);
        barcelona.add (winselona);

        System.out.println (barcelona);

        System.out.println (barcelona.size());

        System.out.println (barcelona.get(1));

        System.out.println (barcelona);

    }
}
