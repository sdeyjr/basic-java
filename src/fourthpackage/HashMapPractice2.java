package fourthpackage;

import java.util.HashMap;

public class HashMapPractice2 {

    public static void main (String [] args) {

        HashMap <Integer, String> barcelona = new HashMap<>();

        barcelona.put (1, "Ter Stegen");
        barcelona.put (9, "Aubamiyang");
        barcelona.put (2, "Dani Alves");
        barcelona.put (5, "Sergio Busquets");
        barcelona.put (6, "Gabi");

        System.out.println (barcelona);

        System.out.println(barcelona.size());

        System.out.println (barcelona.get(9));

        barcelona.remove(2);

        System.out.println (barcelona);

    }
}
