package fourthpackage;

public class Present3 {
    public static void main(String[] args) {
        // if else if statement
        // if statements are executed from the top, going downward
        // when the if statement is true, the code within that block will execute
        // when none of the if statements are deemed true, the else statement will execute

        String sky = "Blue";
        if (sky.equals("Black")) {
            System.out.println("Dark sky");
        } else if (sky.equals("gray")) {
            System.out.println("Cloudy sky");
        } else {
            System.out.println("Normal sky");
        }

        int waist = 35;
        if (waist == 30) {
            System.out.println("Perfect fit");
        } else if (waist == 28) {
            System.out.println("tight fit");
        } else if (waist == 32) {
            System.out.println("loose fit");
        } else {
            System.out.println("Doesnt fit at all");
        }


    }
}