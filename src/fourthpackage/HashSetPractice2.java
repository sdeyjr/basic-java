package fourthpackage;

import java.util.HashSet;

public class HashSetPractice2 {
    public static void main(String[] args) {

        HashSet<String> barcelona = new HashSet<>();

        barcelona.add("adama");
        barcelona.add("pedri");
        barcelona.add("gabi");

        System.out.println(barcelona);

        System.out.println(barcelona.size());

        //System.out.println (barcelona.get(1));

        System.out.println(barcelona.remove(1));

        System.out.println(barcelona);

//        Iterator<String> khobis = barcelona.iterator();
//        while (khobis.hasNext()) {
//            System.out.println(khobis.next());
//
          for (String ele : barcelona) {
                System.out.println(ele + "");
         }
       // barcelona.forEach(System.out::println);

    }

}
