package firstpackage;

import java.util.Scanner;

public class FourthClass {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter a number");
        int num1 = scanner.nextInt();
        for (int i = 0; i <= 10; i++) {
            System.out.println(num1 + "*" + i + "=" + (num1 * i));
        }
    }
}
