package thirdpackage;

public class TeamBarcelonaExecution {
    public static void main(String[] args) {

        TeamBarcelona teamBarcelona = new TeamBarcelona("Pep", 11);

        //1
        TeamBarcelona.nameOfTheClub = "Barcelona";
        String bestTeamInTheWorld = TeamBarcelona.getNameOfTheClub();
        System.out.println(bestTeamInTheWorld);

        //2
        teamBarcelona.coachNow();

        //3
        TeamBarcelona.nameOfManager = "Pep";
        String bestManagerEver = teamBarcelona.getNameOfTheManager();
        System.out.println(bestManagerEver);

        //4
        teamBarcelona.setNameOfTheTrainer("Dembeler khala");
        String worstTrainerEver = teamBarcelona.getNameOfTheTrainer();
        System.out.println(worstTrainerEver);

        //5
        TeamBarcelona.rivalClub = "Khobisor dol madrid";
        String theAbsoluteWorst = TeamBarcelona.rivalClub;
        System.out.println(theAbsoluteWorst);

        //6
        TeamBarcelona.bestPlayer = "Messi the goat";
        String goated = TeamBarcelona.bestPlayer;
        System.out.println(goated);

        //7
        teamBarcelona.setNameOfStriker("Suarez");
        String bestStrikerEver = teamBarcelona.getNameOfTheStriker();
        System.out.println(bestStrikerEver);

        //8
        teamBarcelona.setNameOfPresident("juan");
        String khobisPresident = teamBarcelona.getNameOfPresident();
        System.out.println(khobisPresident);

        //9
        teamBarcelona.setNameOfTheBottle("Poland Spring");
        String shurmaNodirFani = teamBarcelona.getNameOfBottle();
        System.out.println(shurmaNodirFani);

        //10
        teamBarcelona.setNameOfMusic("Un mes song");
        String fosaGaan = teamBarcelona.getNameOfMusic();
        System.out.println(fosaGaan);


    }
}
