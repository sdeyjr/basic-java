package thirdpackage;

public class AstonVilla {


    //    // create 3 static variables and 3 non static variables (with value)
    //    // create 2 static method and 3 non static method
    //    // print the static variables into non static methods
    //    // print the static variables into static method
    //    // print the non static variables into non static methods
    //    // print the non static variable into static methods
    //    // finally call all the methods in the main method


    // create 3 static variables and 3 non static variables (with value)
    public static String michael = "Antonio";
    public static String jarred = "Bowen";
    public static String pablo = "Fornals";
    public String phillip = "Cautinho";
    public String douglas = "Luiz";
    public String tyrone = "Mings";


    //    // create 2 static method and 3 non static method
    public static void rightStriker() {
        System.out.println(michael);
        AstonVilla astonVilla = new AstonVilla();
        System.out.println(astonVilla.douglas);
    }

    public static void leftStriker() {
        System.out.println(jarred);
        AstonVilla astonVilla = new AstonVilla();
        System.out.println(astonVilla.tyrone);
    }

    public void leftWinger() {
        System.out.println(michael);
        System.out.println(phillip);
    }

    public void rightWinger() {
        System.out.println(jarred);
        System.out.println(douglas);
    }

    public void falseNine() {
        System.out.println(pablo);
        System.out.println(tyrone);
    }

    public static void main(String[] args) {
        rightStriker();
        leftStriker();

        AstonVilla astonVilla = new AstonVilla();
        astonVilla.leftWinger();
        astonVilla.rightWinger();
        astonVilla.falseNine();

    }


}
