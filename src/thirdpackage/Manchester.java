package thirdpackage;

public class Manchester {


        public static void myInformation(String name, int age) {
                System.out.println(name + age);
        }

        public static void myInformation(int age) {

        }

        public static void myInformation(String name) {

        }


        public static void myInformation() {
                String firstName = "sub";
                String lastName = "Dey";
                String fullName = firstName + lastName;

                System.out.println(fullName);
        }


        // return type methods --> perform some actions and return back with some data


        public static String getMyInformation() {
                String firstName = "sub";
                String lastName = "Dey";
                String fullName = (firstName + lastName);

                return (fullName);
        }
        public static void main (String [] args) {

                myInformation();
                myInformation("MD", 22);

                String info = getMyInformation();

                System.out.println(info + 22);

                myInformation(10);
        }
}